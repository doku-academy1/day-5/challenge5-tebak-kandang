import java.util.*;

public class Main {
    public static void main(String[] args) {
        Integer menu, jumlahKandang, prediksi;
        String hewan;

        Scanner keyboard = new Scanner(System.in);

        // Membuat menu
        Board.menu();
        menu = keyboard.nextInt();

        // Menanyakan jumlah kandang
        Board.jumlahKandang();
        jumlahKandang = keyboard.nextInt();
        Board board2 = new Board(jumlahKandang);
        for (int i = 1; i<=jumlahKandang; i++) {
            board2.buatkandang(i);
        }

        // Masukkan prediksi hewan
        Board.tebakkan();
        hewan = keyboard.next().toUpperCase();

        //Masukkan prediksi kandang
        Board.pilihanKandang();
        prediksi = keyboard.nextInt();

        // Proses
        Board board1 = new Board(jumlahKandang, hewan, prediksi);
        Map<Integer, String> listAnimals = board1.mapAnimals();
        System.out.println(listAnimals);

        Integer benar = 0;
        Map<Integer, String> listBenar = new HashMap<>();


        while (benar <= listAnimals.size()) {

            if (listAnimals.get(prediksi-1).equals(hewan)) {
                listBenar.put(prediksi, hewan);
                benar += 1;
            }

            if (benar == listAnimals.size()) {
                System.out.println("Selamat Anda Berhasil");
                break;
            }


//            for (int i = 0; i<=jumlahKandang; i++) {
//                if (listBenar.get(i).equals(hewan)) {
//                    board1.bukaKandang(listBenar.get(i));
//                } else {
//                    board1.buatkandang(i);
//                }
//            }

            // Masukkan prediksi hewan
            Board.tebakkan();
            hewan = keyboard.next().toUpperCase();
            board1.setHewan(hewan);

            //Masukkan prediksi kandang
            Board.pilihanKandang();
            prediksi = keyboard.nextInt();
            board1.setPrediksi(prediksi);


        }


        for (Integer value: listBenar.keySet()) {
            board1.bukaKandang(listAnimals.get(value-1));
        }

    }
}