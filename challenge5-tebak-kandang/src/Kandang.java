public class Kandang implements ShowKandang {
    private String hewan;
    private Integer angka, prediksi;

    public Kandang(Integer angka, String hewan, Integer prediksi) {
        this.hewan = hewan;
        this.angka = angka;
        this.prediksi = prediksi;
    }

    public Kandang (Integer prediksi, String hewan) {
        this.prediksi = prediksi;
        this.hewan = hewan;
    }

    public Kandang(Integer angka) {
        this.angka = angka;
    }

    public String getHewan() {
        return hewan;
    }

    public Integer getAngka() {
        return angka;
    }

    public Integer getPrediksi() {
        return prediksi;
    }

    public void setHewan(String hewan) {
        this.hewan = hewan;
    }

    public void setPrediksi(Integer prediksi) {
        this.prediksi = prediksi;
    }

    @Override
    public void buatkandang(Integer angka) {
    }

    public void bukaKandang(String hewan) {
    }

}
