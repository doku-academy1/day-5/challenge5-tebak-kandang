import java.util.*;

public class Board extends Kandang {

    Integer angka;

    public Board(Integer angka, String hewan, Integer prediksi) {
        super(angka, hewan, prediksi);
    }

    public Board(Integer angka) {
        super(angka);
    }

    public Board(Integer prediksi, String hewan) {
        super(prediksi, hewan);
    }

    public static void menu() {
        System.out.print("Pilih menu: ");
    }

    public static void jumlahKandang() {
        System.out.print("Masukkan jumlah kandang: ");
    }

    public static void pilihanKandang() {
        System.out.print("Pilih kandang yang ingin dibuka: ");
    }

    public static void tebakkan() {
        System.out.println("---PILIHAN---");
        System.out.println("K: Kambing");
        System.out.println("Z: Zebra");
        System.out.println("B: Bebek");
        System.out.print("Masukkan tebakkan: ");
    }

    @Override
    public void buatkandang(Integer angka) {
        super.buatkandang(angka);
        System.out.print("| | |");
        System.out.println();
        System.out.printf("| %d |", angka);
        System.out.println();
        System.out.print("| | |");
        System.out.println();
        System.out.println();
    }

    @Override
    public void bukaKandang(String hewan) {
        super.bukaKandang(hewan);
        System.out.print("| | |");
        System.out.println();
        System.out.printf("| %s |", hewan);
        System.out.println();
        System.out.print("| | |");
        System.out.println();
        System.out.println();
    }

    public String random() {
        Random random = new Random();
        String[] animals = {"B", "Z", "K"};
        String randomAnimals = animals[random.nextInt(animals.length)];
        return randomAnimals;
    }

    public List<String> listRandomWords() {
        List<String> listWords = new ArrayList<>();
        for (int i = 0; i<getAngka(); i++) {
            listWords.add(random());
        }
        return listWords;
    }

    public Map<Integer, String> mapAnimals() {
        List<String> list = listRandomWords();
        Map<Integer, String> map = new HashMap<>();
        for (int i = 0; i<list.size(); i++) {
            map.put(i, list.get(i));
        }
        return map;
    }
}
